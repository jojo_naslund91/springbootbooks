package com.example.springbootbooks;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    private static List<Book> bookList = List.of(
            new Book("Harry Potter", "JK rowling"),
            new Book("Sagan om Ringen", "JR Tolkien"),
            new Book("Lida", "Stephen King"),
            new Book("Ernst på krigsstigen", "Ernst Himself"),
            new Book("Ernst smeker upp ett kök", "Ernst The Tinkerere"),
            new Book("Ernst blir förälskad i en sten", "Sommar med Ernst")
    );

    @GetMapping
    public List<Book> all(){

        return bookList;
    }

    @PostMapping
    public static Book createBook(@RequestBody CreateBook createBook){

        return new Book(createBook.getName(), createBook.getAuthor());

    }

}
