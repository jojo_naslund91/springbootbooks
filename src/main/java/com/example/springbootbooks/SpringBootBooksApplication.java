package com.example.springbootbooks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
public class SpringBootBooksApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringBootBooksApplication.class, args);


    }

}
