package com.example.springbootbooks;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

@AllArgsConstructor     // Skapar en constructor med alla argument
@Getter                 // Skapar getters för alla argument
@Setter
@Value
public class CreateBook {

    private String name;
    private String Author;

}
